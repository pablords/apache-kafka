

Link: https://learnk8s.io/kafka-ha-kubernetes


#### Kafka Client


```bash
kubectl run kafka-client --rm -ti --image bitnami/kafka:3.1.0 -- bash
```

#### Producer

```bash
kafka-console-producer.sh \
  --topic test \
  --request-required-acks all \
  --bootstrap-server kafka-0.kafka-svc.kafka.svc.cluster.local:9092,kafka-1.kafka-svc.kafka.svc.cluster.local:9092,kafka-2.kafka-svc.kafka.svc.cluster.local:9092
```

#### Consumer

```bash
kafka-console-consumer.sh \
  --topic test \
  --from-beginning \
  --bootstrap-server kafka-0.kafka-svc.kafka.svc.cluster.local:9092,kafka-1.kafka-svc.kafka.svc.cluster.local:9092,kafka-2.kafka-svc.kafka.svc.cluster.local:9092
```

#### Describe

```bash
kafka-topics.sh --describe \
  --topic test \
  --bootstrap-server kafka-0.kafka-svc.kafka.svc.cluster.local:9092,kafka-1.kafka-svc.kafka.svc.cluster.local:9092,kafka-2.kafka-svc.kafka.svc.cluster.local:9092
```


#### Verificar rede k8s 

```bash
kubectl run -i --tty --image busybox dns-test --restart=Never --rm

nslookup kafka-0.kafka
```



#### Drain

kubectl drain node2 \
  --delete-emptydir-data \
  --force \
  --ignore-daemonsets 
  node/node2 cordoned 
  evicting pod kafka/kafka-0 
  pod/kafka-0 evicted 
  node/node2 evicted


kubectl uncordon node2 node/node2 uncordoned

ssh -o StrictHostKeyChecking=no vagrant@192.168.50.23 sudo rm -rf kafka

sudo kubectl config set-context --current --namespace=kafka

zookeeper-shell.sh zookeeper-svc get /brokers/ids/0
